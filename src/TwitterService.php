<?php
namespace App;

use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Lyrixx\Twitter\Twitter;

class TwitterService
{
    /** @var Twitter */
    protected $twitter;

    public function __construct(array $config)
    {
        $this->twitter = new Twitter(
            $config['consumerKey'],
            $config['consumerSecret'],
            $config['accessToken'],
            $config['accessTokenSecret']
        );
    }

    /**
     * @param ImageInterface $image
     * @return array
     */
    public function updateProfilePicture(ImageInterface $image)
    {
        $response = $this->twitter->query('POST', 'account/update_profile_image', [
            'image' => base64_encode($image->get('jpg')),
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param string $message
     * @return array
     */
    public function postTweet($message)
    {
        $response = $this->twitter->query('POST', 'statuses/update', [
            'status' => $message,
        ]);

        return json_decode($response->getBody(), true);
    }
}
