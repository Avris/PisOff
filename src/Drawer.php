<?php
namespace App;

use Imagine\Gd\Font;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Palette\Color\ColorInterface;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;

class Drawer
{
    /** @var Imagine */
    protected $imagine;

    /** @var Box */
    protected $size;

    /** @var ColorInterface */
    protected $background;

    /** @var Font */
    protected $font;

    public function __construct(array $config)
    {
        $this->imagine = new Imagine();
        $palette = new RGB();
        $this->size = new Box($config['size'], $config['size']);
        $this->background = $palette->color($config['background']);
        $this->font = new Font(
            $config['font']['file'],
            $config['font']['size'],
            $palette->color($config['font']['color'])
        );
    }

    /**
     * @param $number
     * @return ImageInterface
     */
    public function draw($number)
    {
        $image = $this->imagine->create(
            $this->size,
            $this->background
        );

        $this->drawText($image, $number, 0.5, 0.25);
        $this->drawText($image, ':(',    0.5, 0.6, 90);

        return $image;
    }

    protected function drawText(ImageInterface $image, $text, $posX = 0.5, $posY = 0.5, $angle = 0)
    {
        $size = imagettfbbox($this->font->getSize(), $angle, $this->font->getFile(), $text);

        $width = abs($size[4] - $size[0]);
        $height = abs($size[5] - $size[1]);

        $position = new Point(
            ($this->size->getWidth() - $width) * $posX,
            ($this->size->getHeight() - $height) * $posY
        );

        $image->draw()->text($text, $this->font, $position, $angle);
    }
}
