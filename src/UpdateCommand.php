<?php
namespace App;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Command\Command;

class UpdateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('update')
            ->setDefinition([
                new InputArgument('from', InputArgument::OPTIONAL, 'Start date', 'now'),
                new InputArgument('until', InputArgument::OPTIONAL, 'End date', '2019-11-12'),
                new InputOption('upload', 'u')
            ])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $from = new \DateTime($input->getArgument('from'));
        $until = new \DateTime($input->getArgument('until'));
        $config = require __DIR__ . '/../res/config.php';

        $days = $from->diff($until)->days;
        $message = sprintf($config['message'], $days );
        $output->writeln('<info>' . $message . '</info>');

        $output->writeln('Generating image');
        $image = (new Drawer($config['image']))->draw($days);

        if (!$input->getOption('upload')) {
            $filename = $days . '.jpg';
            $image->save(__DIR__ . '/../' . $filename);

            $output->writeln('Saved image to <info>' . $filename . '</info>.');
            $output->writeln('Run with <comment>--upload</comment> to send to Twitter');

            return;
        }

        $twitter = new TwitterService($config['twitter']);

        $output->writeln('Updating profile pic');
        $twitter->updateProfilePicture($image);

        $output->writeln('Posting a tweet');
        $twitter->postTweet($message);
    }
}
