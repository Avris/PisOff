## Licznik dni do końca rządów PiS ##

Przykładowa konfiguracja (plik `res/config.php`):

    <?php return [
        'message' => 'Zostało %s dni do końca kadencji PiS',
        'image' => [
            'size' => 360,
            'background' => '#444',
            'font' => [
                'file' => __DIR__ . '/Ubuntu-B.ttf',
                'color' => '#fff',
                'size' => 80,
            ],
        ],
        'twitter' => [
            'consumerKey' => '{consumerKey}',
            'consumerSecret' => '{consumerSecret}',
            'accessToken' => '{accessToken}',
            'accessTokenSecret' => '{accessToken}',
        ],
    ];

Uruchamianie skryptu:

    ./update --upload
